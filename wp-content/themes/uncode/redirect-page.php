<?php

/**
 * The coming soon template file.
 *
 * @package uncode
 */

get_header();

/** Get general datas **/
$style = ot_get_option('_uncode_general_style');

$redirect_page = ot_get_option('_uncode_redirect_page');
$the_content = get_post_field('post_content', $redirect_page);
if (has_shortcode($the_content, 'vc_row'))
{
	$the_content = '<div class="post-content">' . $the_content . '</div>';
}
else
{
	$the_content = apply_filters('the_content', $the_content);
	$the_content = '<div class="post-content">' . uncode_get_row_template($the_content, '', '', $style, '', 'double', true, 'double') . '</div>';
}
/** Display post html **/
echo 	'<article id="post-'. get_the_ID().'" class="'.implode(' ', get_post_class('page-body style-'.$style.'-bg')) .'">
				<div class="post-wrapper">
					<div class="post-body">' . do_shortcode($the_content) . '</div>
				</div>
			</article>';
get_footer(); ?>