<?php
$style = $is_header = $slider_type = $slider_loop = $el_class = $limit_content = $top_padding = $bottom_padding = $h_padding = $slider_height = $output = '';
extract(shortcode_atts(array(
	'style' => '',
	'is_header' => '',
	'slider_type' => '',
	'slider_loop' => '',
	'el_class' => '',
	'limit_content' => '',
	'top_padding' =>'',
	'bottom_padding' =>'',
	'h_padding' =>'',
	'slider_height' => '',
) , $atts));

/** send variable to inner columns **/
if ($limit_content === 'yes') {
	$content = str_replace('[vc_row_inner','[vc_row_inner limit_content="yes" override_padding="yes" top_padding="'.$top_padding.'" bottom_padding="'.$bottom_padding.'" ', $content);
} else {
	$content = str_replace('[vc_row_inner','[vc_row_inner override_padding="yes" top_padding="'.$top_padding.'" bottom_padding="'.$bottom_padding.'" h_padding="'.$h_padding.'" ', $content);
}

$numSlides = substr_count($content, '[vc_row_inner');

if ($slider_type === 'fade') $slider_type = ' data-fade="true"';

$el_id = 'uslider_' . rand();

$el_class = $this->getExtraClass($el_class);
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'uncode-slider ' . $el_class, $this->settings['base'], $atts );

$style = $is_header !== 'true' ? ' style-'.$style : '';

$output .= '<div class="owl-carousel-wrapper'.$style.'">';
$output .= '<div class="'.esc_attr($css_class).'owl-carousel-container">';
$output .= '<div id="'.esc_attr($el_id).'" class="owl-carousel owl-element'.($is_header === 'true' ? ' owl-dots-inside' : ' owl-dots-outside').' owl-height-'.esc_attr($slider_height).'"'.$slider_type.' data-loop="'.($slider_loop === 'yes' ? "true" : "false").'" data-autoheight="'.($slider_height === 'auto' ? 'true' : 'false').'"'.($is_header === 'true' ? ' data-nav="true"' : ' data-dotsmobile="true"').' data-dots="true" data-navspeed="400" data-autoplay="false" data-lg="1" data-md="1" data-sm="1">';
$output .= $content;
$output .= '</div>';
$output .= '</div>';
$output .= '</div>';

echo wpb_js_remove_wpautop($output);