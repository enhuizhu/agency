<?php
/*
  Plugin Name: Custom Contact
  Plugin URI: http://www.olmarket.co.uk
  Description: custom contact for olmarket.co.uk.
  Version: 1.0
  Author: enhui zhu
  Author URI: http://www.olmarket.co.uk
 */
add_action("wp_ajax_sendMail", "sendMail");
add_action("wp_ajax_nopriv_sendMail", "sendMail");
if(!function_exists("sendMail")){
     function sendMail(){
          /**
          * should check if capcha is ok
          **/
          session_start();

          if ($_SESSION["capcha"] != $_POST["capcha"]) {
            echo json_encode(array("success" => false, "message" => "capcha is wrong!"));
            die();
          }

          $to = 'zhuen2000@163.com';
          
          $subject = 'Project Request';
          
          $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";
          $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          $headers .= "CC: sunlilyy@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

          $message = '<html><head></head><body>
            <table border="1">
              <tr>
              <td>Name</td><td>'.$_POST["name"].'</td>
              </tr>
              <tr>
              <td>Email</td><td>'.$_POST["email"].'</td>
              </tr>
              <tr>
              <td>Message</td><td>'.$_POST["message"].'</td>
              </tr>
              <tr>
              <td>Telephone</td><td>'.$_POST["telephone"].'</td>
              </tr>
              <tr>
              <td>Currency</td><td>'.$_POST["currency"].'</td>
              </tr>
              <tr>
              <td>Price</td><td>'.$_POST["price"].'</td>
              </tr>
            </table>
          </body></html>';

          if (mail($to, $subject, $message, $headers)) {
              echo json_encode(array("success" => true, "message" => "the request has been sent successfully!"));
          }else{
              echo json_encode(array("success" => false, "message" => "some error happend"));
          }
          die();
     }
}

add_filter('widget_text', 'do_shortcode');

function contactForm() {
    /**
    * get the plugin url 
    **/
    $content = '<script>
      var ajaxUrl = "'.get_site_url().'/wp-admin/admin-ajax.php";
    </script>';
    /**
    * add plugin script
    **/
    $content .= '<script src="'.plugins_url('scripts/contact.js', __FILE__).'"></script>';
    // $content .= "<link rel='stylesheet' type='text/css' href='".plugins_url("styles/contactForm.css", __FILE__)."'/>";
    $content .= '<style type="text/css">
      .contact-form-wrapper{
      }

      .info-wrapper {
        float: left;
        font-size: 14px;
      }

      .section {
        min-width: 300px;
        text-align: left;
        margin-top: 26px
      }

      .section label{
        font-weight: bolder;
        display: inline-block;
        width: 145px;
        text-align: right;
      }

      .section span {
        float: right;
        width: 114px;
      }

      .form-wrapper {
        float: right;
        max-width: 50%;
      }

      @media only screen 
        and (max-width: 640px) 
      {
        .info-wrapper{
          float: none;
          display: block;
          width: 300px;
        }

        .section label{
          text-align: left;
        }

        .section span{
          width: 150px;
        }

        .form-wrapper{
          float: none;
          display: block;
          max-width: 100%;
        }
        .clear{
          clear: both;
        }
      }
    </style>';
    $content .='
       <div role="form" class="f7">
        <div class="screen-reader-response"></div>
        <form action="" method="post" class="wpcf7-form" novalidate="novalidate" id="contactForm">
        <div style="display: none;">
        </div>
        <p><span class="wpcf7-form-control-wrap your-fullname"><input type="text" name="your-fullname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-fluid" aria-required="true" aria-invalid="false" placeholder="Your full name"></span></p>
        <div class="wpcf7-inline-wrapper">
        <p class="wpcf7-inline-field"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your Email"></span> </p>
        <p class="wpcf7-inline-field"><span class="wpcf7-form-control-wrap your-telephone"><input type="text" name="your-telephone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-fluid" aria-required="true" aria-invalid="false" placeholder="Phone"></span> </p>
        </div>
        <p><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="8" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Describe your project"></textarea></span> </p>
        <p><span class="wpcf7-form-control-wrap menu-640">
         <select name="currency">
          <option value="pound">Pound(&pound;)</option>
          <option value="dollar">Dollar($)</option>
          <option value="yuan">Yuan(&yen;)</option>
         </select>
        </span></p>
        <p><span class="wpcf7-form-control-wrap menu-640">
        <select name="project-price" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
         <option value="500 - 3,000">500 - 3,000</option><option value="3,000 - 10,000">3,000 - 10,000</option><option value="10,000 - 15,000">10,000 - 15,000</option><option value="15,000 +">15,000 +</option>
        </select></span>
        </p>
        <div class="wpcf7-inline-wrapper">
        <p class="wpcf7-inline-field"><span class="wpcf7-form-control-wrap your-email"><input type="text" name="capcha" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Capcha"></span> </p>
        <p class="wpcf7-inline-field"><img src="'.plugins_url("capcha.php", __FILE__).'" align="left" style="position:relative; top:11px;"/></p>
        </div>
        <p>
         <input type="submit" value="Send Request" class="wpcf7-form-control wpcf7-submit btn btn-accent btn-circle"><img class="ajax-loader" src="http://prelive.olmarket.co.uk/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
        </p>
        <div class="wpcf7-response-output wpcf7-display-none">here is response</div></form>
        </div>
      ';

    echo $content;
}

// Register a new shortcode: [cr_custom_registration]
add_shortcode('olmarket_contact', 'custom_contact_shortcode');

// The callback function that will replace [book]
function custom_contact_shortcode() {
    ob_start();
    contactForm();
    return ob_get_clean();
}
