/**
* contact.js
**/
jQuery(document).ready(function() {
	var setError = function(domObj)	{
		domObj.css({'border': 'solid 1px red'});
	}

	var removeError = function(domObj) {
		domObj.css({'border':''});
	}

	jQuery("#contactForm").submit(function(e) {
		var that = this;
		e.preventDefault();
		/**
		* get all the field value
		**/
		var name = jQuery(this).find("[name=your-fullname]").val(),
			email = jQuery(this).find("[name=your-email]").val(),
			telephone = jQuery(this).find("[name=your-telephone]").val(),
			message = jQuery(this).find("[name=your-message]").val(),
			currency = jQuery(this).find("[name=currency]").val(),
			price = jQuery(this).find("[name=project-price]").val(),
			capcha = jQuery(this).find("[name=capcha]").val();
			hasError = false;

		if (name == "") {
			setError(jQuery(this).find("[name=your-fullname]"));
			hasError = true;
		};

		if (!/.+@.+/.test(email)) {
			setError(jQuery(this).find("[name=your-email]"));
			hasError = true;
		};

		if (!/\d/.test(telephone)) {
			setError(jQuery(this).find("[name=your-telephone]"));
			hasError = true;
		};

		if (message == "") {
			setError(jQuery(this).find("[name=your-message]"));
			hasError = true;
		};

		if (capcha == "") {
			setError(jQuery(this).find("[name=capcha]"));
			hasError = true;
		};


		if (hasError) {
			return false;
		};

		/**
		* every thing is fine, send ajax request
		**/
		var data = "action=sendMail&name="+name+"&email="+email+"&telephone="+telephone+"&message="+message+"&price="+price+"&currency="+currency+"&capcha="+capcha;	

		jQuery.ajax({
			url: ajaxUrl,
			type: "post",
			data: data,
			beforeSend: function(xhr) {
				jQuery(that).find(".ajax-loader").css({"visibility":"visible"});
			},
			success: function(response) {
				jQuery(that).find(".ajax-loader").css({"visibility":"hidden"});
				jQuery(that).find(".wpcf7-response-output").removeClass("wpcf7-display-none");
				
				var jsonRep = JSON.parse(response);

				jQuery(that).find(".wpcf7-response-output").html(jsonRep.message);

				if (jsonRep.success) {
					/**
					* reset the the form
					**/
					jQuery(that)[0].reset();
				}
			}
		});
	});

	jQuery("#contactForm input,#contactForm textarea").focus(function(e) {
		console.info("focus");
		removeError(jQuery(this));
	});

});