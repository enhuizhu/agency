<?php
/**
* php code to generate capcha
**/

/**
* function to generate 4 random characters
**/
function generateFourRandomLetters() {
	$rangeStr = "1234567890abcdefghijklmnopqrst";
	$len = strlen($rangeStr);
	$letters = array();

	for ($i=0; $i <4 ; $i++) { 
		$randomNumber = rand(0, $len - 1);
		array_push($letters, substr($rangeStr, $randomNumber, 1));
	}

	return implode("", $letters);
}

session_start();

$_SESSION["capcha"] = generateFourRandomLetters();

// Create a 100*30 image
$im = imagecreate(100, 30);

// White background and blue text
$bg = imagecolorallocate($im, 255, 255, 255);
$textcolor = imagecolorallocate($im, 0, 0, 255);

// Write the string at the top left
imagestring($im, 5, 0, 0, $_SESSION["capcha"], $textcolor);

// Output the image
header('Content-type: image/png');

imagepng($im);

imagedestroy($im);
