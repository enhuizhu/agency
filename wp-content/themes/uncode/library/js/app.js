

/* ----------------------------------------------------------
 * Uncode App
 * ---------------------------------------------------------- */

(function($) {
	"use strict";
	var UNCODE = window.UNCODE || {};
	window.UNCODE = UNCODE;

UNCODE.utils = function() {
	$('.btn-tooltip').tooltip();
	$('a').hover(function() {
		$(this).attr('data-title', $(this).attr('title'));
		$(this).removeAttr('title');
	}, function() {
		$(this).attr('title', $(this).attr('data-title'));
	});
	$('.counter').counterUp({
		delay: 10,
		time: 1500
	});
	$('[data-countdown]').each(function() {
		var $this = $(this),
			finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('%D <small>days</small> %H <small>hours</small> %M <small>minutes</small> %S <small>seconds</small>'));
		});
	});
	var share_button_top = new Share(".share-button", {
		ui: {
			flyout: "top center",
			button_font: false,
			button_text: '',
			icon_font: false
		},
	});
	$(document).off('click', '.dot-irecommendthis');
	$(document).on('click', '.dot-irecommendthis', function() {
		var link = $(this),
			linkCounter = link.find('span');
		if (link.hasClass('active')) return false;
		var id = $(this).attr('id'),
			suffix = link.find('.dot-irecommendthis-suffix').text();
		$.post(dot_irecommendthis.ajaxurl, {
			action: 'dot-irecommendthis',
			recommend_id: id,
			suffix: suffix
		}, function(data) {
			var counter = $(data).text();
			linkCounter.html(counter);
			link.addClass('active').attr('title', 'You already recommended this');
		});
		return false;
	});
	$('a').on('click', function(e) {
		var hash = (e.currentTarget).hash,
		is_scrolltop = $(e.currentTarget).hasClass('scroll-top') ? true : false,
		anchor = '';
		if (hash != undefined) anchor = hash.replace(/^#/, "");
		if (is_scrolltop || anchor != '') {
			if (is_scrolltop) {
				e.preventDefault();
				var bodyTop = document.documentElement['scrollTop'] || document.body['scrollTop'],
				scrollSpeed = Math.abs(bodyTop) / 2;
				if (scrollSpeed < 1000) scrollSpeed = 1000;
				$('html, body').animate({
					scrollTop: 0
				}, scrollSpeed, 'easeInOutCubic', function() {
				});
			} else {
				var scrollSection = $('[data-name=' + anchor + ']');
				$.each($('.menu-container .menu-item > a'), function(index, val) {
					if ($(val).attr('href') == '#' + anchor) $(val).parent().addClass('active');
					else $(val).parent().removeClass('active');
				});
				if (scrollSection.length) {
					e.preventDefault();
					UNCODE.scrolling = true;
					var bodyTop = document.documentElement['scrollTop'] || document.body['scrollTop'],
						delta = bodyTop - scrollSection.offset().top,
						scrollSpeed = Math.abs(delta) / 2;
					if (scrollSpeed < 1000) scrollSpeed = 1000;
					$('html, body').animate({
						scrollTop: scrollSection.offset().top
					}, scrollSpeed, 'easeInOutCubic', function() {
						UNCODE.scrolling = false;
					});
				}
			}
		}
	});
	$('.header-scrolldown').on('click', function(event) {
		event.preventDefault();
		var pageHeader = $(event.target).closest('#page-header'),
			pageHeaderTop = pageHeader.offset().top,
			pageHeaderHeight = pageHeader.outerHeight(),
			scrollSpeed = Math.abs(pageHeaderTop + pageHeaderHeight) / 2;
		if (scrollSpeed < 1000) scrollSpeed = 1000;
		$('html, body').animate({
			scrollTop: pageHeaderTop + pageHeaderHeight
		}, scrollSpeed, 'easeInOutCubic');

	});
	// TAB DATA-API
	// ============
	$(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function(e) {
		e.preventDefault()
		$(this).tab('show');
		setTimeout(function() {
			window.dispatchEvent(UNCODE.boxEvent);
		}, 300);
	});
	// COLLAPSE DATA-API
	// =================
	$(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function(e) {
		var $this = $(this),
			href
		var target = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
		var $target = $(target)
		var data = $target.data('bs.collapse')
		var option = data ? 'toggle' : $this.data()
		var parent = $this.attr('data-parent')
		var $parent = parent && $(parent)
		var $title = $(this).parent()
		if ($parent) {
			$parent.find('[data-toggle="collapse"][data-parent="' + parent + '"]').not($this).addClass('collapsed')
			$parent.find('.panel-title').removeClass('active')
			$title[!$target.hasClass('in') ? 'addClass' : 'removeClass']('active')
		}
		$this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
			// }
		$target.collapse(option)
	});
	// FitText
	// =================
	window.uncode_textfill = function(el, loaded) {
		if (el == undefined) el = $('body');
		$.each($('.bigtext', el), function(index, val) {
			$(val).bigtext({
				minfontsize: 24
			});
			if (!$(val).parent().hasClass('blocks-animation') && !$(val).hasClass('animate_when_almost_visible')) $(val).css({
				opacity: 1
			});
		});
	}
	window.uncode_textfill();

	// Colomun hover effect
	// =================
	$(document).on('mouseenter', '.col-link', function(e) {
		var uncol = $(e.target).prev('.uncol');
		el = uncol.find('.column-background');
		if (el) {
			$('.btn-container .btn', uncol).toggleClass('active');
			var elOverlay = $(el[0]).find('.block-bg-overlay');
			if (elOverlay.length) {
				var getOpacity = $(elOverlay).css('opacity');
				if (getOpacity != 1) {
					getOpacity = Math.round(getOpacity * 100) / 100;
					var newOpacity = getOpacity + .1;
					$(elOverlay).data('data-opacity', getOpacity);
					$(elOverlay).css('opacity', newOpacity);
				}
			}
		}
	}).on('mouseleave', '.col-link', function(e) {
		var uncol = $(e.target).prev('.uncol');
		el = uncol.find('.column-background');
		$('.btn-container .btn', uncol).toggleClass('active');
		if (el) {
			var elOverlay = $(el[0]).find('.block-bg-overlay');
			if (elOverlay.length) {
				var getOpacity = $(elOverlay).data('data-opacity');
				$(elOverlay).css('opacity', getOpacity);
			}
		}
	});
}

UNCODE.menuSystem = function() {
	function menuMobileButton() {
		var $mobileToggleButton = $('.mobile-menu-button')
		var open = false;
		$mobileToggleButton.on('click', function(event) {
			event.preventDefault();
			if ($(window).width() < 960) {
				if (!open) {
					this.classList.add('close');
					open = true;
				} else {
					this.classList.remove('close');
					open = false;
				}
			}
		});
	};

	function menuMobile() {
		var $mobileToggleButton = $('.mobile-menu-button'),
			$box,
			$el,
			elHeight,
			check,
			animating = false;
		$mobileToggleButton.on('click', function(event) {
			event.preventDefault();
			if ($(window).width() < UNCODE.mediaQuery) {
				$box = $(this).closest('.box-container').find('.main-menu-container'),
					$el = $(this).closest('.box-container').find('.menu-horizontal-inner, .menu-sidebar-inner');
				elHeight = 0;
				$.each($el, function(index, val) {
					elHeight += $(val).outerHeight();
				});
				var open = function() {
					if (!animating) {
						animating = true;
						$box.animate({
							height: elHeight
						}, 400, "easeInOutCirc", function() {
							$box.css('height', 'auto');
						});
					}
				};

				var close = function() {
					$box.animate({
						height: 0
					}, {
						duration: 400,
						easing: "easeInOutCirc",
						complete: function(elements) {
							$(elements).css('height', '');
							animating = false;
						}
					});
				};

				check = ($box.height() == 0) ? open() : close();
			}
		});
	};

	function menuOffCanvas() {
		$('.menu-primary .menu-button-offcanvas').click(function(event) {
			if ($(window).width() > UNCODE.mediaQuery) {
				if ($(event.currentTarget).hasClass('overlay-close')) $(event.currentTarget).removeClass('overlay-close');
				else $(event.currentTarget).addClass('overlay-close');
			}
			$('body').toggleClass('off-opened');
		});
	};
	function menuSmart() {
		if ($('[class*="menu-smart"]').length > 0) {
			$('[class*="menu-smart"]').smartmenus({
				subIndicators: false,
				subIndicatorsPos: 'append',
				subMenusMinWidth: '13em',
				subIndicatorsText: '',
				showTimeout: 50,
				hideTimeout: 50,
				showFunction: function($ul, complete) {
					$ul.fadeIn(0, 'linear', complete);
				},
				hideFunction: function($ul, complete) {
					var fixIE = $('html.ie').length;
					if (fixIE) {
						var $rowParent = $($ul).closest('.main-menu-container');
						$rowParent.height('auto');
					}
					$ul.fadeOut(0, 'linear', complete);
				},
				collapsibleShowFunction: function($ul, complete) {
					$ul.slideDown(400, 'easeInOutCirc', function() {
					});
				},
				collapsibleHideFunction: function($ul, complete) {
					$ul.slideUp(200, 'easeInOutCirc', complete);
				},
				hideOnClick: false
			});
		}
	};
	menuMobileButton();
	menuMobile();
	menuOffCanvas();
	menuSmart();
};

UNCODE.okvideo = function() {
	var BLANK_GIF = "data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw%3D%3D";
	$.okvideo = function(options) {
		// if the option var was just a string, turn it into an object
		if (typeof options !== 'object') options = {
			'video': options
		};
		var base = this;
		// kick things off
		base.init = function() {
			base.options = $.extend({}, $.okvideo.options, options);
			// support older versions of okvideo
			if (base.options.video === null) base.options.video = base.options.source;
			base.setOptions();
			var target = base.options.target || $('body');
			var position = target[0] == $('body')[0] ? 'fixed' : 'absolute';
			var zIndex = base.options.controls === 3 ? -999 : "auto";
			if ($('#okplayer-' + base.options.id).length == 0) { //base.options.id = String(Math.round(Math.random() * 100000));
				var mask = '<div id="okplayer-mask-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:-998;height:100%;width:100%;"></div>';
				if (OKEvents.utils.isMobile()) {
					target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:' + zIndex + ';height:100%;width:100%;"></div>');
				} else {
					if (base.options.controls === 3) {
						target.append(mask)
					}
					if (base.options.adproof === 1) {
						target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:-10%;top:-10%;overflow:hidden;z-index:' + zIndex + ';height:120%;width:120%;"></div>');
					} else {
						target.append('<div id="okplayer-' + base.options.id + '" style="position:' + position + ';left:0;top:0;overflow:hidden;z-index:' + zIndex + ';height:100%;width:100%;"></div>');
					}
				}
				$("#okplayer-mask-" + base.options.id).css("background-image", "url(" + BLANK_GIF + ")");
				if (base.options.playlist.list === null) {
					if (base.options.video.provider === 'youtube') {
						base.loadYouTubeAPI();
					} else if (base.options.video.provider === 'vimeo') {
						base.options.volume /= 100;
						base.loadVimeoAPI();
					}
				} else {
					base.loadYouTubeAPI();
				}
			}
		};
		// clean the options
		base.setOptions = function() {
			// exchange 'true' for '1' and 'false' for 3
			for (var key in this.options) {
				if (this.options[key] === true) this.options[key] = 1;
				if (this.options[key] === false) this.options[key] = 3;
			}
			if (base.options.playlist.list === null) {
				base.options.video = base.determineProvider();
			}
			// pass options to the window
			$(window).data('okoptions-' + base.options.id, base.options);
		};
		// insert js into the head and exectue a callback function
		base.insertJS = function(src, callback){
      var tag = document.createElement('script');
      if (callback){
        if (tag.readyState){  //IE
          tag.onreadystatechange = function(){
            if (tag.readyState === "loaded" ||
                tag.readyState === "complete"){
              tag.onreadystatechange = null;
              callback();
            }
          };
        } else {
          tag.onload = function() {
            callback();
          };
        }
      }
      tag.src = src;
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    };
		// load the youtube api
		base.loadYouTubeAPI = function(callback) {
			base.insertJS('//www.youtube.com/player_api');
		};
		base.loadYouTubePlaylist = function() {
			player.loadPlaylist(base.options.playlist.list, base.options.playlist.index, base.options.playlist.startSeconds, base.options.playlist.suggestedQuality);
		};
		// load the vimeo api by replacing the div with an iframe and loading js
		base.loadVimeoAPI = function() {
			var source = '//player.vimeo.com/video/' + base.options.video.id + '?api=1&title=0&byline=0&portrait=0&playbar=0&loop=' + base.options.loop + '&autoplay=' + (base.options.autoplay === 1 ? 1 : 0) + '&player_id=okplayer-' + base.options.id,
			jIframe = $('<iframe data-src="'+source+'" frameborder="0" id="okplayer-' + base.options.id +'" style="visibility: hidden;" class="vimeo-background" />');
			$(window).data('okoptions-' + base.options.id).jobject = jIframe;
			$('#okplayer-' + base.options.id).replaceWith(jIframe[0]);
			base.insertJS('//origin-assets.vimeo.com/js/froogaloop2.min.js', function() {
				vimeoPlayerReady(base.options.id);
			});
		};
		// is it from youtube or vimeo?
		base.determineProvider = function() {
			var a = document.createElement('a');
			a.href = base.options.video;
			if (/youtube.com/.test(base.options.video) || /youtu.be/.test(base.options.video)) {
				var videoid = a.href.split('/')[3].toString();
				var query = videoid.substring(videoid.indexOf('?') + 1);
				if (query != '') {
					var vars = query.split('&');
					for (var i = 0; i < vars.length; i++) {
						var pair = vars[i].split('=');
						if (pair[0] == 'v') {
							videoid = pair[1];
						}
					}
				}
				return {
					"provider": "youtube",
					"id": videoid
				};
			} else if (/vimeo.com/.test(base.options.video)) {
				return {
					"provider": "vimeo",
					"id": (a.href.split('/')[3].toString()).split('#')[0],
				};
			} else if (/[-A-Za-z0-9_]+/.test(base.options.video)) {
				var id = new String(base.options.video.match(/[-A-Za-z0-9_]+/));
				if (id.length == 11) {
					return {
						"provider": "youtube",
						"id": id.toString()
					};
				} else {
					for (var i = 0; i < base.options.video.length; i++) {
						if (typeof parseInt(base.options.video[i]) !== "number") {
							throw 'not vimeo but thought it was for a sec';
						}
					}
					return {
						"provider": "vimeo",
						"id": base.options.video
					};
				}
			} else {
				throw "OKVideo: Invalid video source";
			}
		};
		base.init();
	};
	$.okvideo.options = {
		id: null,
		source: null, // Deprecate dis l8r
		video: null,
		playlist: { // eat ur heart out @brokyo
			list: null,
			index: 0,
			startSeconds: 0,
			suggestedQuality: "default" // options: small, medium, large, hd720, hd1080, highres, default
		},
		disableKeyControl: 1,
		captions: 0,
		loop: 1,
		hd: 1,
		volume: 0,
		adproof: false,
		unstarted: null,
		onFinished: null,
		onReady: null,
		onPlay: null,
		onPause: null,
		buffering: null,
		controls: false,
		autoplay: true,
		annotations: true,
		cued: null
	};
	$.fn.okvideo = function(options) {
		options.target = this;
		return this.each(function() {
			(new $.okvideo(options));
		});
	};

	$(".no-touch .uncode-video-container.video").each(function(index, el) {
		var $this = $(this),
			url = $this.attr('data-video'),
			id = $this.attr('data-id'),
			cloned = $this.closest('.owl-item');
		if (!cloned.hasClass('cloned') || cloned.length == 0) {
			$this.okvideo({
				id: id,
				source: url.split('#')[0],
				time: ((url).indexOf("#") > -1) ? (url).substring((url).indexOf('#') + 1) : null,
				autoplay: 1,
				controls: 0,
				volume: 0,
				adproof: 0,
				caller: $this,
				hd: 1,
				onReady: function(player) {
					var getPlayer = player.f || player,
					getContainer = $(getPlayer).closest('.background-element');
					if (getContainer.length) {
						UNCODE.initVideoComponent(getContainer[0], '.uncode-video-container.video');
					}
				}
			});
		}
	});
	$(".no-touch .background-video-shortcode").each(function(index, el) {
		var video_id = $(this).attr('id');
		new MediaElement(video_id, {
			startVolume: 0,
			loop: true,
			success: function(mediaElement, domObject) {
				domObject.volume = 0;
				$(mediaElement).data('started', false);
				mediaElement.addEventListener('timeupdate', function(e) {
					if (!$(e.target).data('started')) {
						$(mediaElement).data('started', true);
						$(mediaElement).closest('.uncode-video-container').css('opacity', '1');
					}
				});
				mediaElement.play();
			},
			// fires when a problem is detected
			error: function() {}
		});
	});
};

UNCODE.disableHoverScroll = function() {

    if (!UNCODE.isMobile) {
        var body = document.body,
        timer;

        window.addEventListener('scroll', function() {
            clearTimeout(timer);
            if (body.classList)  {
                if (!body.classList.contains('disable-hover')) {
                    body.classList.add('disable-hover')
                }

                timer = setTimeout(function() {
                    body.classList.remove('disable-hover')
                }, 300);
            }
        }, false);
    }
};


UNCODE.isotopeLayout = function() {
	if ($('.isotope-layout').length > 0) {
		var isotopeContainersArray = [],
			typeGridArray = [],
			layoutGridArray = [],
			screenLgArray = [],
			screenMdArray = [],
			screenSmArray = [],
			transitionDuration = [],
			$filterItems = [],
			$filters = $('.isotope-filters'),
			$itemSelector = '.tmb',
			$items,
			itemMargin,
			correctionFactor = 0,
			firstLoad = true,
			isOriginLeft = $('body').hasClass('rtl') ? false : true;
		$('[class*="isotope-container"]').each(function() {
			var isoData = $(this).data(),
			$data_lg,
			$data_md,
			$data_sm;
			if (isoData.lg !== undefined) $data_lg = $(this).attr('data-lg');
			else $data_lg = '1000';
			if (isoData.md !== undefined) $data_md = $(this).attr('data-md');
			else $data_md = '600';
			if (isoData.sm !== undefined) $data_sm = $(this).attr('data-sm');
			else $data_sm = '480';
			screenLgArray.push($data_lg);
			screenMdArray.push($data_md);
			screenSmArray.push($data_sm);
			transitionDuration.push($('.t-inside.animate_when_almost_visible', this).length > 0 ? 0 : '0.5s');
			if (isoData.type == 'metro') typeGridArray.push(true);
			else typeGridArray.push(false);
			if (isoData.layout !== undefined) layoutGridArray.push(isoData.layout);
			else layoutGridArray.push('masonry');
			isotopeContainersArray.push($(this));
		});
		var colWidth = function(index) {
				$(isotopeContainersArray[index]).width('');
				var isPx = $(isotopeContainersArray[index]).parent().hasClass('px-gutter'),
					widthAvailable = $(isotopeContainersArray[index]).width(),
					columnNum = 12,
					columnWidth = 0;

				if (isPx) {
					columnWidth = Math.ceil(widthAvailable / columnNum);
					$(isotopeContainersArray[index]).width(columnNum * Math.ceil(columnWidth));
				} else {
					columnWidth = ($('html.firefox').length) ? Math.floor(widthAvailable / columnNum) : widthAvailable / columnNum;
				}
				$items = $(isotopeContainersArray[index]).find('.tmb:not(.tmb-carousel)');
				itemMargin = parseInt($(isotopeContainersArray[index]).find('.t-inside').css("margin-top"));
				for (var i = 0, len = $items.length; i < len; i++) {
					var $item = $($items[i]),
						multiplier_w = $item.attr('class').match(/tmb-iso-w(\d{0,2})/),
						multiplier_h = $item.attr('class').match(/tmb-iso-h(\d{0,2})/);
					if (widthAvailable >= screenMdArray[index] && widthAvailable < screenLgArray[index]) {
						if (multiplier_w[1] !== undefined) {
							switch (parseInt(multiplier_w[1])) {
								case (5):
								case (4):
								case (3):
									if (typeGridArray[index]) multiplier_h[1] = (6 * multiplier_h[1]) / multiplier_w[1];
									multiplier_w[1] = 6;
									break;
								case (2):
								case (1):
									if (typeGridArray[index]) multiplier_h[1] = (3 * multiplier_h[1]) / multiplier_w[1];
									multiplier_w[1] = 3;
									break;
								default:
									if (typeGridArray[index]) multiplier_h[1] = (12 * multiplier_h[1]) / multiplier_w[1];
									multiplier_w[1] = 12;
									break;
							}
						}
					} else if (widthAvailable >= screenSmArray[index] && widthAvailable < screenMdArray[index]) {
						if (multiplier_w[1] !== undefined) {
							switch (parseInt(multiplier_w[1])) {
								case (5):
								case (4):
								case (3):
								case (2):
								case (1):
									if (typeGridArray[index]) multiplier_h[1] = (6 * multiplier_h[1]) / multiplier_w[1];
									multiplier_w[1] = 6;
									break;
								default:
									if (typeGridArray[index]) multiplier_h[1] = (12 * multiplier_h[1]) / multiplier_w[1];
									multiplier_w[1] = 12;
									break;
							}
						}
					} else if (widthAvailable < screenSmArray[index]) {
						if (multiplier_w[1] !== undefined) {
							//if (typeGridArray[index]) multiplier_h[1] = (12 * multiplier_h[1]) / multiplier_w[1];
							multiplier_w[1] = 12;
							if (typeGridArray[index]) multiplier_h[1] = 12;
						}
					}
					var width = multiplier_w ? (columnWidth * multiplier_w[1]) : columnWidth,
						height = multiplier_h ? Math['ceil']((2 * Math.ceil(columnWidth / 2)) * multiplier_h[1]) - itemMargin : columnWidth;

					if (width >= widthAvailable) {
						$item.css({
							width: widthAvailable
						});
						if (typeGridArray[index]) {
							$item.children().add($item.find('.backimg')).css({
								height: height
							});
						}
					} else {
						$item.css({
							width: width
						});
						if (typeGridArray[index]) {
							$item.children().add($item.find('.backimg')).css({
								height: height
							});
						}
					}
				}
				return columnWidth;
			},
			init_isotope = function() {
				for (var i = 0, len = isotopeContainersArray.length; i < len; i++) {
					var isotopeSystem = $(isotopeContainersArray[i]).closest($('.isotope-system')),
						isotopeId = isotopeSystem.attr('id'),
						$layoutMode = layoutGridArray[i];
					$(isotopeContainersArray[i]).isotope({
						//resizable: true,
						itemSelector: $itemSelector,
						layoutMode: $layoutMode,
						transitionDuration: transitionDuration[i],
						masonry: {
							columnWidth: colWidth(i)
						},
						vertical: {
							horizontalAlignment: 0.5,
						},
						sortBy: 'original-order',
						isOriginLeft: isOriginLeft
					}).on('layoutComplete', onLayout($(isotopeContainersArray[i]), 0));
					if ($(isotopeContainersArray[i]).hasClass('isotope-infinite')) {
						$(isotopeContainersArray[i]).infinitescroll({
								navSelector: '#' + isotopeId + ' .loadmore-button', // selector for the pagination container
								nextSelector: '#' + isotopeId + ' .loadmore-button a', // selector for the NEXT link (to page 2)
								itemSelector: '#' + isotopeId + ' .isotope-layout .tmb, #' + isotopeId + ' .isotope-filters li', // selector for all items you'll retrieve
								animate: false,
								behavior: 'local',
								debug: false,
								loading: {
									selector: '#' + isotopeId + '.isotope-system .isotope-footer-inner',
									speed: 0,
									finished: undefined,
									msg: $('#' + isotopeId + ' .loadmore-button'),
								},
								errorCallback: function() {
									$('#' + isotopeId + '.isotope-system .loading-button').hide();
									$('#' + isotopeId + '.isotope-system .loadmore-button').attr('style', 'display:none !important');
								}
							},
							// append the new items to isotope on the infinitescroll callback function.
							function(newElements) {
								var $isotope = $(this),
									isotopeId = $isotope.closest('.isotope-system').attr('id'),
									filters = new Array(),
									$loading_button = $isotope.closest('.isotope-system').find('.loading-button'),
									$infinite_button = $isotope.closest('.isotope-system').find('.loadmore-button'),
									delay = 300;
								$('a', $infinite_button).html($('a', $infinite_button).data('label'));
								$infinite_button.show();
								$loading_button.hide();
								$('> li', $isotope).remove();
								$.each($(newElements), function(index, val) {
									if ($(val).is("li")) {
										filters.push($(val)[0]);
									}
								});
								newElements = newElements.filter(function(x) {
									return filters.indexOf(x) < 0
								});
								$.each($(filters), function(index, val) {
									if ($('#' + isotopeId + ' a[data-filter="' + $('a', val).attr('data-filter') + '"]').length == 0) $('#' + isotopeId + ' .isotope-filters ul').append($(val));
								});
								$isotope.isotope('reloadItems', onLayout($isotope, newElements.length));
							});
						if ($(isotopeContainersArray[i]).hasClass('isotope-infinite-button')) {
							var $infinite_isotope = $(isotopeContainersArray[i]),
								$infinite_button = $infinite_isotope.closest('.isotope-system').find('.loadmore-button a');
							$infinite_isotope.infinitescroll('pause');
							$infinite_button.on('click', function(event) {
								event.preventDefault();
								isotopeId = $(event.target).closest('.isotope-system').attr('id');
								$(event.currentTarget).html('Loading…');
								$infinite_isotope.infinitescroll('resume');
								$infinite_isotope.infinitescroll('retrieve');
								$infinite_isotope.infinitescroll('pause');
							});
						}
					}
				}
			},
			onLayout = function(isotopeObj, startIndex) {
				isotopeObj.css('opacity', 1);
				isotopeObj.closest('.isotope-system').find('.isotope-footer').css('opacity', 1);
				setTimeout(function() {
					window.dispatchEvent(UNCODE.boxEvent);
					$(isotopeObj).find('audio,video').each(function() {
						$(this).mediaelementplayer();
					});
					if ($(isotopeObj).find('.nested-carousel').length) {
						UNCODE.carousel($(isotopeObj).find('.nested-carousel'));
						setTimeout(function() {
							boxAnimation($('.tmb', isotopeObj), startIndex, true, isotopeObj);
						}, 200);
					} else {
						boxAnimation($('.tmb', isotopeObj), startIndex, true, isotopeObj);
					}
				}, 100);
			},
			boxAnimation = function(items, startIndex, sequential, container) {
				var $allItems = items.length - startIndex,
					showed = 0,
					index = 0;
				if (container.closest('.owl-item').length == 1) return false;
				$.each(items, function(index, val) {
					var elInner = $('> .t-inside', val);
					if (val[0]) val = val[0];
					if (elInner.hasClass('animate_when_almost_visible') && !elInner.hasClass('force-anim') && !UNCODE.isMobile) {
						new Waypoint({
							element: val,
							handler: function() {
								var element = $('> .t-inside', this.element),
									parent = $(this.element),
									currentIndex = parent.index();
								var delay = (!sequential) ? index : ((startIndex !== 0) ? currentIndex - $allItems : currentIndex),
									delayAttr = parseInt(element.attr('data-delay'));
								if (isNaN(delayAttr)) delayAttr = 100;
								delay -= showed;
								var objTimeout = setTimeout(function() {
									element.removeClass('zoom-reverse').addClass('start_animation');
									showed = parent.index();
								}, delay * delayAttr)
								parent.data('objTimeout', objTimeout);
								this.destroy();
							},
							offset: '100%'
						})
					} else {
						if (elInner.hasClass('force-anim')) {
							elInner.addClass('start_animation');
						} else {
							elInner.css('opacity', 1);
						}
					}
					index++;
				});
			};
		if ($('.isotope-pagination').length > 0) {
			var filterHeight = ($filters != undefined) ? $filters.outerHeight(true) - $('a', $filters).first().height() : 0;
			$('.isotope-system').on('click', '.pagination a', function(evt) {
				evt.preventDefault();
				var container = $(this).closest('.isotope-system');
				$('html, body').animate({
					scrollTop: container.offset().top - filterHeight
				}, 1000, 'easeInOutQuad');
				loadIsotope($(this));
				evt.preventDefault();
			});
		}
		$filters.on('click', 'a', function(evt) {
			var $filter = $(this),
				filterContainer = $filter.closest('.isotope-filters'),
				filterValue = $filter.attr('data-filter'),
				container = $filter.closest('.isotope-system').find($('.isotope-layout')),
				transitionDuration = container.data().isotope.options.transitionDuration,
				delay = 300,
				filterItems = [];
			if (!$filter.hasClass('active')) {
				/** Scroll top with filtering */
				if (filterContainer.hasClass('filter-scroll')) {
					$('html, body').animate({
						scrollTop: container.closest('.uncol').offset().top - (!filterContainer.hasClass('with-bg') ? filterContainer.outerHeight(true) - $filter.height() : 0)
					}, 1000, 'easeInOutQuad');
				}
				if (filterValue !== undefined) {
					$.each($('> .tmb > .t-inside', container), function(index, val) {
						var parent = $(val).parent(),
							objTimeout = parent.data('objTimeout');
						if (objTimeout) {
							$(val).removeClass('zoom-reverse').removeClass('start_animation')
							clearTimeout(objTimeout);
						}
						if (transitionDuration == 0) {
							if ($(val).hasClass('animate_when_almost_visible')) {
								$(val).addClass('zoom-reverse').removeClass('start_animation');
							} else {
								$(val).addClass('animate_when_almost_visible zoom-reverse zoom-anim force-anim');
							}
						}
					});
					setTimeout(function() {
						container.isotope({
							filter: function() {
								var block = $(this),
								filterable = (filterValue == '*') || block.hasClass(filterValue);
								if (filterable) {
									filterItems.push(block);
								}
								return filterable;
							}
						});
						$('.t-inside.zoom-reverse', container).removeClass('zoom-reverse');
					}, delay);
					/** once filtered - start **/
					if (transitionDuration == 0) {
						container.isotope('once', 'arrangeComplete', function() {
							setTimeout(function() {
								boxAnimation(filterItems, 0, false, container);
							}, 100);
						});
					}
					/** once filtered - end **/
				} else {
					$.each($('> .tmb > .t-inside', container), function(index, val) {
						var parent = $(val).parent(),
							objTimeout = parent.data('objTimeout');
						if (objTimeout) {
							$(val).removeClass('zoom-reverse').removeClass('start_animation')
							clearTimeout(objTimeout);
						}
						if (transitionDuration == 0) {
							if ($(val).hasClass('animate_when_almost_visible')) {
								$(val).addClass('zoom-reverse').removeClass('start_animation');
							} else {
								$(val).addClass('animate_when_almost_visible zoom-reverse zoom-anim force-anim');
							}
						}
					});
					container.parent().addClass('isotope-loading');
					loadIsotope($filter);
				}
			}
			evt.preventDefault();
		});
		$(window).on("popstate", function(e) {
			if (e.originalEvent.state === null) return;
			var params = {};
			if (location.search) {
				var parts = location.search.substring(1).split('&');
				for (var i = 0; i < parts.length; i++) {
					var nv = parts[i].split('=');
					if (!nv[0]) continue;
					params[nv[0]] = nv[1] || true;
				}
			}
			if (params.id === undefined) {
				$.each($('.isotope-system'), function(index, val) {
					loadIsotope($(val));
				});
			} else loadIsotope($('#' + params.id));
		});

		var loadIsotope = function($href) {
			var href = ($href.is("a") ? $href.attr('href') : location),
				isotopeSystem = ($href.is("a") ? $href.closest($('.isotope-system')) : $href),
				isotopeWrapper = isotopeSystem.find($('.isotope-wrapper')),
				isotopeFooter = isotopeSystem.find($('.isotope-footer-inner')),
				isotopeContainer = isotopeSystem.find($('.isotope-layout')),
				isotopeId = isotopeSystem.attr('id');
			if ($href.is("a")) history.pushState({
				myIsotope: true
			}, document.title, href);
			$.ajax({
				url: href
			}).done(function(data) {
				var $resultItems = $(data).find('#' + isotopeId + ' .isotope-layout').html(),
					$resultPagination = $(data).find('#' + isotopeId + ' .pagination');
				isotopeWrapper.addClass('isotope-reloaded');
				setTimeout(function() {
					isotopeWrapper.removeClass('isotope-loading');
					isotopeWrapper.removeClass('isotope-reloaded');
				}, 500);
				$.each($('> .tmb > .t-inside', isotopeContainer), function(index, val) {
					var parent = $(val).parent(),
						objTimeout = parent.data('objTimeout');
					if (objTimeout) {
						$(val).removeClass('zoom-reverse').removeClass('start_animation')
						clearTimeout(objTimeout);
					}
					if ($(val).hasClass('animate_when_almost_visible')) {
						$(val).addClass('zoom-reverse').removeClass('start_animation');
					} else {
						$(val).addClass('animate_when_almost_visible zoom-reverse zoom-in force-anim');
					}
				});
				setTimeout(function() {
					if (isotopeContainer.data('isotope')) {
						isotopeContainer.html($resultItems).isotope('reloadItems', onLayout(isotopeContainer, 0));
					}
				}, 300);
				$('.pagination', isotopeFooter).remove();
				isotopeFooter.append($resultPagination);
			});
		};
		$filters.each(function(i, buttonGroup) {
			var $buttonGroup = $(buttonGroup);
			$buttonGroup.on('click', 'a', function() {
				$buttonGroup.find('.active').removeClass('active');
				$(this).addClass('active');
			});
		});
		window.addEventListener('boxResized', function(e) {
			$.each($('.isotope-layout'), function(index, val) {
				var $layoutMode = ($(this).data('layout'));
				if ($layoutMode === undefined) $layoutMode = 'masonry';
				if ($(this).data('isotope')) {
					$(this).isotope({
						itemSelector: $itemSelector,
						layoutMode: $layoutMode,
						transitionDuration: transitionDuration[index],
						masonry: {
							columnWidth: colWidth(index)
						},
						vertical: {
							horizontalAlignment: 0.5,
						},
						sortBy: 'original-order',
						isOriginLeft: isOriginLeft
					});
					$(this).isotope('unbindResize');
				}
				$(this).find('.mejs-video,.mejs-audio').each(function() {
					$(this).trigger('resize');
				});
			});
		}, false);
		init_isotope();
	};
}

UNCODE.lightbox = function() {
	setTimeout(function() {
		var groupsArr = {};
		$('[data-lbox^=ilightbox]').each(function() {
			var group = this.getAttribute("data-lbox"),
				values = $(this).data();
			groupsArr[group] = values;
		});
		for (var i in groupsArr) {
			var skin = groupsArr[i].skin || 'black',
				path = groupsArr[i].dir || 'horizontal',
				thumbs = !groupsArr[i].notmb || false,
				arrows = !groupsArr[i].noarr || false,
				social = groupsArr[i].social || false,
				deeplink = groupsArr[i].deep || false,
				counter = $('[data-lbox="' + i + '"]').length;
			if (social) social = {
				facebook: true,
				twitter: true,
				googleplus: true,
				reddit: true,
				digg: true,
				delicious: true
			};
			$('[data-lbox="' + i + '"]').iLightBox({
				skin: skin,
				path: path,
				linkId: deeplink,
				infinite: false,
				//fullViewPort: 'fit',
				smartRecognition: false,
				fullAlone: false,
				//fullStretchTypes: 'flash, video',
				overlay: {
					blur: false,
					opacity: .94
				},
				controls: {
					arrows: (counter > 1 ? arrows : false),
					fullscreen: true,
					thumbnail: thumbs,
					slideshow: (counter > 1 ? true : false)
				},
				show: {
					speed: 200
				},
				hide: {
					speed: 200
				},
				social: {
					start: false,
					buttons: social
				},
				caption: {
					start: false
				},
				styles: {
					nextOpacity: 1,
					nextScale: 1,
					prevOpacity: 1,
					prevScale: 1
				},
				effects: {
					switchSpeed: 400
				},
				slideshow: {
					pauseTime: 5000
				},
				thumbnails: {
					maxWidth: 60,
					maxHeight: 60,
					activeOpacity: .2
				},
				html5video: {
					preload: true
				}
			});
		};
	}, 100);
};

UNCODE.backVideo = function() {
	$(function() {
		$.each($('.background-video-shortcode'), function() {
			var video_id = $(this).attr('id');
			new MediaElement(video_id, {
				startVolume: 0,
				loop: true,
				success: function(mediaElement, domObject) {
					mediaElement.play();
					$(mediaElement).closest('.uncode-video-container').css('opacity','1');
					domObject.volume = 0;
				},
				// fires when a problem is detected
				error: function() {}
			});
		});
	});
};

UNCODE.carousel = function(container) {
	var $owlSelector = $('.owl-carousel-container > [class*="owl-carousel"]', container),
		values = {},
		tempTimeStamp,
		currentIndex,
		$owlInsideEqual = [];
	$owlSelector.each(function() {
		var itemID = $(this).attr('id'),
			$elSelector = $(('#' + itemID).toString());
		values['id'] = itemID;
		values['items'] = 1;
		values['columns'] = 3;
		values['fade'] = false;
		values['nav'] = false;
		values['navmobile'] = false;
		values['navskin'] = 'light';
		values['navspeed'] = 400;
		values['dots'] = false;
		values['dotsmobile'] = false;
		values['loop'] = false;
		values['autoplay'] = false;
		values['timeout'] = 3000;
		values['autoheight'] = false;
		//values['rtl'] = false;
		values['margin'] = 0;
		values['lg'] = 1;
		values['md'] = 1;
		values['sm'] = 1;
		$.each($(this).data(), function(i, v) {
			values[i] = v;
		});

		if ($(this).closest('.uncode-slider').length) {
			values['navskin'] = '';
			values['navmobile'] = false;
			values['dotsmobile'] = true;
		} else {
			values['navskin'] = ' style-'+values['navskin']+' style-override';
		}

		/** Initialized */
		$elSelector.on('initialized.owl.carousel', function(event) {

			var thiis = $(event.currentTarget),
				// get the time from the data method
				time = thiis.data("timer-id");
			if (time) {
				clearTimeout(time);
			}
			thiis.addClass('showControls');
			var new_time = setTimeout(function() {
				thiis.closest('.owl-carousel-container').removeClass('owl-carousel-loading');
				if (thiis.hasClass('owl-height-equal')) setItemsHeight(event.currentTarget);
				if (!UNCODE.isMobile && !$elSelector.closest('.header-wrapper').length) navHover($elSelector.parent());
			}, 350);
			// save the new time
			thiis.data("timer-id", new_time);

			var scrolltop = $(document).scrollTop();
			//navAdjust(event);
			$(event.currentTarget).closest('.uncode-slider').find('video').removeAttr('poster');

			if (!UNCODE.isMobile) {
				setTimeout(function() {
					/** fix autoplay when visible **/
					if ($(event.currentTarget).data('autoplay')) {
						$(event.currentTarget).trigger('stop.autoplay.owl');
					}
					new Waypoint({
						element: $(event.currentTarget),
						handler: function() {
							var el = $(this.element);
							if (el.data('autoplay')) {
								setTimeout(function() {
									el.trigger('play.owl.autoplay');
								}, values['timeout']);
							}
						},
						offset: '100%'
					});
					new Waypoint({
						element: $(event.currentTarget),
						handler: function() {
							if ($(this.element).data('autoplay')) {
								$(this.element).trigger('stop.owl.autoplay');
							}
						},
						offset: '-20%'
					});
				}, 1000);
			}

			if (!UNCODE.isMobile && !$(event.currentTarget).closest('.isotope-system').length) {
				setTimeout(function() {
					animate_thumb($('.t-inside', el));
				}, 400);
			}

			var currentItem = $(event.currentTarget).find("> .owl-stage-outer > .owl-stage > .owl-item")[event.item.index],
			currentIndex = $(currentItem).attr('data-index');
			$.each($('.owl-item:not(.active)', event.currentTarget), function(index, val) {
				if ($(val).attr('data-index') != currentIndex) {
					$('.start_animation:not(.t-inside)', val).removeClass('start_animation');
				}
				if ($(val).attr('data-index') == currentIndex) {
					$('.animate_when_almost_visible:not(.t-inside)', val).addClass('start_animation');
				}
			});
			$.each($('.owl-item:not(.active) .start_animation', $(event.target)), function(index, val) {
				if ($(val).closest('.uncode-slider').length) {
					$(val).removeClass('start_animation');
				}
			});

			if ($(event.currentTarget).closest('.uncode-slider').length) {
				var el = $(event.currentTarget).closest('.row-parent')[0];
				if ($(el).data('imgready')) {
					firstLoaded(el);
				} else {
					el.addEventListener("imgLoaded", function(el) {
						firstLoaded(el.target);
					}, false);
				}
				var transHeight = $('.hmenu .menu-transparent.menu-primary .menu-container').height();
				if (transHeight != null) {
					setTimeout(function() {
						$(event.currentTarget).closest('.uncode-slider').find('.owl-prev, .owl-next').css('paddingTop', transHeight / 2 + 'px');
					}, 100);
				}
			} else {
				var el = $(event.currentTarget);
				el.closest('.uncode-slider').addClass('slider-loaded');
			}

			setTimeout(function() {
				window.uncode_textfill(thiis);
			}, 500);

			if ($(event.currentTarget).closest('.unequal').length) {
				$owlInsideEqual.push($(event.currentTarget).closest('.row-parent'));
			}

		});

		/** Resizing */
		$elSelector.on('resized.owl.carousel', function(event) {
			if ($(this).closest('.nested-carousel').length) {
				setTimeout(function() {
					window.dispatchEvent(UNCODE.boxEvent);
				}, 200);
			}
			if ($(event.currentTarget).hasClass('owl-height-equal')) setItemsHeight(event.currentTarget);
		});

		/** Change */
		$elSelector.on('change.owl.carousel', function(event) {
			if (!UNCODE.isMobile) UNCODE.owlStopVideo(event.currentTarget);
		});

		/** Changed */
		$elSelector.on('changed.owl.carousel', function(event) {
			if (tempTimeStamp != event.timeStamp) {
				var scrolltop = $(document).scrollTop();
				var currentItem = $(event.currentTarget).find("> .owl-stage-outer > .owl-stage > .owl-item")[(event.item.index != null) ? event.item.index : 0];
				if ($(event.currentTarget).closest('.row-slider').length) {
					if (currentItem == undefined) {
						currentItem = $(event.currentTarget).children()[0];
					}
					if ($('.row-container > .row > .row-inner > div > .style-dark', currentItem).closest('.uncode-slider').length) {
						UNCODE.switchColorsMenu(scrolltop, 'dark');
					} else if ($('.row-container > .row > .row-inner > div > .style-light', currentItem).closest('.uncode-slider').length) {
						UNCODE.switchColorsMenu(scrolltop, 'light');
					}
				}
			}
			tempTimeStamp = event.timeStamp;
		});

		$elSelector.on('translate.owl.carousel', function(event) {
			if (UNCODE.isMobile) {
				$(event.currentTarget).addClass('owl-translating');
			}
		});

		/** Translated */
		$elSelector.on('translated.owl.carousel', function(event) {
			if (!UNCODE.isMobile) {
				UNCODE.owlPlayVideo(event.currentTarget);

				setTimeout(function() {
					animate_elems($('.owl-item.active', event.currentTarget));
					animate_thumb($('.owl-item.active .t-inside', event.currentTarget));
				}, 200);

				var currentItem = $(event.currentTarget).find("> .owl-stage-outer > .owl-stage > .owl-item")[event.item.index],
				currentIndex = $(currentItem).attr('data-index');
				$.each($('.owl-item:not(.active)', event.currentTarget), function(index, val) {
					if ($(val).attr('data-index') != currentIndex) {
						$('.start_animation:not(.t-inside)', val).removeClass('start_animation');
					}
					if ($(val).attr('data-index') == currentIndex) {
						$('.animate_when_almost_visible:not(.t-inside)', val).addClass('start_animation');
					}
				});
				$.each($('.owl-item:not(.active) .start_animation', $(event.target)), function(index, val) {
					if ($(val).closest('.uncode-slider').length) {
						$(val).removeClass('start_animation');
					}
				});
			} else {
				$(event.currentTarget).removeClass('owl-translating');
			}
		});

		/** Init carousel */
		$elSelector.owlCarousel({
			items: values['items'],
			animateOut: (values['fade'] == true) ? 'fadeOut' : null,
			nav: values['nav'],
			dots: values['dots'],
			loop: values['loop'],
			margin: 0,
			video: true,
			autoWidth: false,
			autoplay: false,
			//autoplay: (UNCODE.isMobile ? false : values['autoplay']),
			autoplayTimeout: values['timeout'],
			autoplaySpeed: values['navspeed'],
			autoplayHoverPause: true,
			autoHeight: $(this).hasClass('owl-height-equal') ? true : values['autoheight'],
			rtl: $('body').hasClass('rtl') ? true : false,
			fluidSpeed: true,
			navSpeed: values['navspeed'],
			navClass: [ 'owl-prev'+values['navskin'], 'owl-next'+values['navskin'] ],
			navText: ['<div class="owl-nav-container btn-default btn-hover-nobg"><i class="fa fa-fw fa-angle-left"></i></div>', '<div class="owl-nav-container btn-default btn-hover-nobg"><i class="fa fa-fw fa-angle-right"></i></div>'],
			navContainer: values['nav'] ? $elSelector : false,
			responsiveClass: true,
			responsiveBaseElement: '.box-container',
			responsive: {
				0: {
					items: values['sm'],
					nav: values['navmobile'],
					dots: values['dotsmobile']
				},
				480: {
					items: values['sm'],
					nav: values['navmobile'],
					dots: values['dotsmobile']
				},
				570: {
					items: values['md'],
					nav: values['navmobile'],
					dots: values['dotsmobile']
				},
				960: {
					items: values['lg']
				}
			},
		});

		setTimeout(function() {
			for (var i = $owlInsideEqual.length - 1; i >= 0; i--) {
				UNCODE.setRowHeight($owlInsideEqual[i]);
			};
		}, 300);
	});

	function firstLoaded(el) {
		var el = $(el);
		el.find('.owl-carousel').css('opacity', 1);
		el.find('.uncode-slider').addClass('slider-loaded');
		window.uncode_textfill(el.find('.owl-item.active'));

		if (!UNCODE.isMobile) {
			setTimeout(function() {
				animate_elems(el.find('.owl-item.active'));
				animate_thumb(el.find('.owl-item.active .t-inside'));
			}, 400);
		}
	}

	function navHover(el) {
		var $owlCont = el,
			$owlPrev = $owlCont.find('.owl-prev'),
			$owlNext = $owlCont.find('.owl-next'),
			$owlDots = $owlCont.find('.owl-dots-inside .owl-dots'),
			$owlPagination = $owlCont.next(),
			owlPrevW = $owlPrev.outerWidth(),
			owlNextW = $owlNext.outerWidth(),
			owlDotsH = $owlDots.innerHeight(),
			owlTime = 400,
			owlNested = $owlCont.parent().parent().hasClass('nested-carousel');
		$owlPrev.css("margin-left", -owlPrevW);
		$owlNext.css("margin-right", -owlNextW);
		if (!owlNested) $owlDots.css("bottom", -owlDotsH);
		$owlCont.mouseenter(function() {
			owlNested = $owlCont.parent().parent().hasClass('nested-carousel');
			$owlPrev.css({
				marginLeft: 0
			});
			$owlNext.css({
				marginRight: 0
			});
			if (!owlNested) {
				$owlDots.css({
					opacity: 1,
					bottom: 0
				});
			}
		}).mouseleave(function() {
			owlNested = $owlCont.parent().parent().hasClass('nested-carousel');
			$owlPrev.css({
				marginLeft: -owlPrevW
			});
			$owlNext.css({
				marginRight: -owlNextW
			});
			if (!owlNested) {
				$owlDots.css({
					opacity: 1,
					bottom: -owlDotsH
				});
			}
		});
	};

	function animate_elems($this) {
		$.each($('.animate_when_almost_visible:not(.t-inside)', $this), function(index, val) {
			var element = $(val),
				//index = element.index(),
				delayAttr = element.attr('data-delay');
			if (delayAttr == undefined) delayAttr = 0;
			setTimeout(function() {
				element.addClass('start_animation');
			}, delayAttr);
		});
	}

	function animate_thumb(items) {
		var currentIndex = 0;
			$.each(items, function(index, val) {
				var parent = $(val).closest('.owl-item');
				if (!$(val).hasClass('start_animation')) {
					if (parent.hasClass('active')) {
						new Waypoint({
							element: val,
							handler: function() {
								var element = $(this.element),
									//delay = parent.index(),
									delayAttr = parseInt(element.attr('data-delay'));
								if (isNaN(delayAttr)) delayAttr = 100;
								var objTimeout = setTimeout(function() {
									element.addClass('start_animation');
								}, currentIndex * delayAttr);
								currentIndex++;
								parent.data('objTimeout', objTimeout);
								this.destroy();
							},
							offset: '100%',
						});
					}
				}
			});
		}

	function setItemsHeight(item) {
		$.each($('.owl-item', item), function(index, val) {
			var availableThumbHeight = $('.t-inside', $(val)).height(),
			innerThumbHeight = $('.t-entry-text-tc', $(val)).outerHeight(),
			difference = availableThumbHeight - innerThumbHeight;
			if ($('.tmb-content-under', val).length) {
				var visualPart = $('.t-entry-visual', val);
				if (visualPart.length) {
					difference -= $('.t-entry-visual', val).height();
				}
			}
			$('.t-entry > *:last-child', val).css( 'transform', 'translateY('+difference+'px)' );
		});
	}
};

UNCODE.owlPlayVideo = function(carousel) {
	var player, iframe;
	$('.owl-item.active .uncode-video-container', carousel).each(function(index, val) {
		var content = $(val).html();
		if (content == '') {
			var getCloned = $('.owl-item:not(.active) .uncode-video-container[data-id="'+$(this).attr('data-id')+'"]').children().first().clone();
			$(val).append(getCloned);
		}
		if ($(this).attr('data-provider') == 'vimeo') {
			iframe = $(this).find('iframe');
			player = $f(iframe[0]);
			player.api('play');
		} else if ($(this).attr('data-provider') == 'youtube') {
			youtubePlayers[$(this).attr('data-id')].playVideo();
		} else {
			var player = $(this).find('video');
			if (player.length) {
				$(this).find('video')[0].volume = 0;
				$(this).find('video')[0].play();
				$(val).css('opacity', 1);
			}
		}
	});
};

UNCODE.owlStopVideo = function(carousel) {
	$('.owl-item .uncode-video-container', carousel).each(function(index, val) {
		var player, iframe;
		if ($(this).attr('data-provider') == 'vimeo') {
			iframe = $(this).find('iframe');
			player = $f(iframe[0]);
			player.api('pause');
		} else if ($(this).attr('data-provider') == 'youtube') {
			youtubePlayers[$(this).attr('data-id')].pauseVideo();
		} else {
			var player = $(this).find('video');
			if (player.length) {
				$(this).find('video')[0].volume = 0;
				$(this).find('video')[0].play();
			}
		}
	});
};

UNCODE.animations = function() {
	if (!UNCODE.isMobile) {
		$.each($('.header-content-inner'), function(index, val) {
			var element = $(val),
				transition = '';
			if (element.hasClass('top-t-bottom')) transition = 'top-t-bottom';
			if (element.hasClass('bottom-t-top')) transition = 'bottom-t-top';
			if (element.hasClass('left-t-right')) transition = 'left-t-right';
			if (element.hasClass('right-t-left')) transition = 'right-t-left';
			if (element.hasClass('zoom-in')) transition = 'zoom-in';
			if (element.hasClass('zoom-out')) transition = 'zoom-out';
			if (element.hasClass('alpha-anim')) transition = 'alpha-anim';
			if (transition != '') {
				$(val).removeClass(transition);
				var container = element,
					containerDelay = container.attr('data-delay'),
					containerSpeed = container.attr('data-speed'),
					items = $('.header-title > *, .post-info', container);
				$.each(items, function(index, val) {
					var element = $(val),
						//speedAttr = (containerSpeed == undefined) ? containerSpeed : '',
						delayAttr = (containerDelay != undefined) ? containerDelay : 400;
					if (!element.hasClass('animate_when_almost_visible')) {
						delayAttr = Number(delayAttr) + (400 * index);
						if (containerSpeed != undefined) element.attr('data-speed', containerSpeed);
						element.addClass(transition + ' animate_when_almost_visible').attr('data-delay', delayAttr);
					}
				});
				container.css('opacity', 1);
			}
		});
	}

	if (!window.waypoint_animation) {
		window.waypoint_animation = function() {
			$.each($('.animate_when_almost_visible:not(.start_animation):not(.t-inside), .tmb-media .animate_when_almost_visible:not(.start_animation)'), function(index, val) {
				var run = true;
				if ($(val).closest('.owl-carousel').length > 0) run = false;
				if (run) {
					new Waypoint({
						element: val,
						handler: function() {
							var element = $(this.element),
								index = element.index(),
								delayAttr = element.attr('data-delay');
							if (delayAttr == undefined) delayAttr = 0;
							setTimeout(function() {
								element.addClass('start_animation');
							}, delayAttr);
							this.destroy();
						},
						offset: '90%'
					});
				}
			});
		}
	}
	setTimeout(function() {
		if (!UNCODE.isMobile) window.waypoint_animation();
	}, 100);
}

UNCODE.tapHover = function() {

    var $el = $('html.touch .t-entry-visual-cont > a'), //.length //html.touch a.btn
        elClass = "hover";

    $el.on("click", function(e) { // cambia click con touch start 'touchstart'
        var link = $(this);
        if (link.hasClass(elClass)) {
            return true;
        } else {
            link.addClass("hover");
            $el.not(this).removeClass(elClass);
            e.preventDefault();
            return false;
        }
    });
};


UNCODE.onePage = function() {
	var current = 0,
		last = 0,
		lastScrollTop = 0,
		forceScroll = false,
		lastScrolled = 0,
		isSectionscroller = ($('.main-onepage').length) ? true : false;

	function init_onepage() {
		if (isSectionscroller) {
			$("<ul class='onepage-pagination'></ul>").prependTo("body");
		}
		last = $('.onepage-section').length - 1;
		$.each($('div[data-parent=true]'), function(index, val) {
			$(this).attr('data-section', index);
			new Waypoint({
				element: val,
				handler: function() {
					current = lastScrolled = parseInt($(this.element).attr('data-section'));
					if (isSectionscroller) {
						$('ul.onepage-pagination li a').removeClass('is-selected');
						$('.onepage-pagination li a[data-index=' + index + ']').addClass('is-selected');
					}
					var getName = $('[data-section=' + index + ']').attr('data-name');
					if (getName != undefined) {
						$.each($('.menu-container .menu-item > a'), function(i, val) {
							if ($(val).attr('href') == '#' + getName) {
								$(val).closest('ul').find('.active').removeClass('active');
								$(val).parent().addClass('active');
							}
						});
					}
				},
			});

			if (isSectionscroller) {
				var label;
				if ($(this).attr('data-label') != undefined) label = $(this).attr('data-label');
				else label = '';
				var getName = $(this).attr('data-name');
				if (getName == undefined) getName = index;
				if (label != '') label = '<span class="cd-label style-accent-bg border-accent-color">' + label + '</span>';
				$('ul.onepage-pagination').append("<li><a data-index='" + (index) + "' href='#" + (getName) + "'><span class='cd-dot-cont'><span class='cd-dot'></span></span>"+label+"</a></li>");
			}
		});

		if (isSectionscroller) {
			$.each($('ul.onepage-pagination li'), function(index, val) {
				var $this = $(val);
				$this.on('click', function(evt) {
					var el = $('a', evt.currentTarget);
					current = lastScrolled = parseInt(el.attr('data-index'));
					lastScrolled += 1;
					scrollBody(current);
				});
			});
			var goToSection = parseInt((window.location.hash).replace(/[^\d.]/g, ''));
			if (isNaN(goToSection) && window.location.hash != undefined && window.location.hash != '') {
				goToSection = String(window.location.hash).replace(/^#/, "");
				goToSection = Number($('[data-name=' + goToSection + ']').attr('data-section'));
			}
			if (typeof goToSection === 'number' && !isNaN(goToSection)) {
				current = lastScrolled = goToSection;
				setTimeout(function() {
					scrollBody(goToSection);
				}, 500);
			}
		}
	}

	if (isSectionscroller) {
		$(window).on('scroll', function() {
			var bodyTop = document.documentElement['scrollTop'] || document.body['scrollTop'];
			if (bodyTop == 0) {
				$('ul.onepage-pagination li a').removeClass('is-selected');
				$('.onepage-pagination li a[data-index=0]').addClass('is-selected');
				var getName = $('[data-section=0]').attr('data-name');
				if (getName != undefined) {
					$.each($('.menu-container .menu-item > a'), function(i, val) {
						if ($(val).attr('href') == '#' + getName) {
							$(val).closest('ul').find('.active').removeClass('active');
							$(val).parent().addClass('active');
						}
					});
				}
			} else if ((window.innerHeight + bodyTop) >= $('.box-container').height()) {
				$('ul.onepage-pagination li a').removeClass('is-selected');
				$('.onepage-pagination li a[data-index="' + last +'"]').addClass('is-selected');
			}
		});

		var scrollBody = function(index) {
			$('ul.onepage-pagination li a').removeClass('is-selected');
			$('.onepage-pagination li a[data-index=' + index + ']').addClass('is-selected');
			var body = $("html, body"),
			bodyTop = document.documentElement['scrollTop'] || document.body['scrollTop'],
			delta = bodyTop - $('[data-section=' + index + ']').offset().top,
			scrollTo = $('[data-section=' + index + ']').offset().top;
			if (index != 0) {
				UNCODE.scrolling = true;
			}
			body.animate({
				scrollTop: (delta > 0) ? scrollTo - 0.1 : scrollTo
			}, Math.abs(delta) / 2, 'easeInOutQuad', function() {
				setTimeout(function(){
					UNCODE.scrolling = false;
				}, 100);
			});
		};
	}

	init_onepage();
};

	UNCODE.init = function() {
		UNCODE.utils();
		UNCODE.menuSystem();
		UNCODE.okvideo();
		UNCODE.tapHover();
		UNCODE.isotopeLayout();
		UNCODE.lightbox();
		UNCODE.backVideo();
		UNCODE.carousel($('body'));
		UNCODE.animations();
		UNCODE.disableHoverScroll();
		if (!UNCODE.isMobile) {
			UNCODE.onePage();
		}
	}
	UNCODE.init();
})(jQuery);

